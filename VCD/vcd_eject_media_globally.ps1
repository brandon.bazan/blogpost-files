﻿# Based on https://community.broadcom.com/vmware-cloud-foundation/discussion/accessing-vcloud-rest-api-with-powershell from Comment4 Doug Baer's invoke-restmethod in VCD
# VCD Media added on by Brandon Bazan
# Configure REST authentication
# Variables to be defined:
$vcdHost = 'lvn-cm3-vcd1.broadcom.com'

# vCloud API version and accept header
$apiver = "39.0"
$accept_header = 'application/*+json;version=39.0'

# Obtain Username and password
$vcdCredential = Get-Credential -Message "Please enter user and password"

# For a system admin, you can use the org named system
$vcdOrg = 'system'

# Connect to VCD
Connect-CIServer -Server $vcdHost -Org $vcdOrg -User $vcdCredential.UserName -Password $vcdCredential.Password

$SecurityProtocols = @(
	[System.Net.SecurityProtocolType]::Ssl3,
	[System.Net.SecurityProtocolType]::Tls,
	[System.Net.SecurityProtocolType]::Tls12
)
[System.Net.ServicePointManager]::SecurityProtocol = $SecurityProtocols -join ","
Write-Host "Adding security protocol Tls,Tls12,Ssl3"

$auth = $vcdCredential.UserName + '@' + $vcdOrg + ':' + $vcdCredential.Password

# Encode basic authorization for the header
$Encoded = [System.Text.Encoding]::UTF8.GetBytes($auth)
$EncodedPassword = [System.Convert]::ToBase64String($Encoded)

# Define a standard header
$headers = @{"Accept"="application/*;version=$apiver"}

# vCD will allow an unauthenticated GET to the https://VCDSERVER/api/versions URL. We'll do that:
$baseurl = "https://$vcdHost/api"

# Create full address of resource to be retrieved
$url = $baseurl + '/versions'

# Unauthenticated GET: request and display API versions supported
$versions = Invoke-RestMethod -Uri $url -Headers $headers -Method Get
ForEach ($ver in $versions.SupportedVersions.VersionInfo) { Write $ver.Version}

# Need to get the login URL
$versions = Invoke-RestMethod -Uri $url -Headers $headers -Method GET

ForEach ($ver in $versions.SupportedVersions.VersionInfo) {

  if ($ver.Version -eq $apiver) { $loginUrl = $ver.ProviderLoginUrl }

}

if($loginUrl -ne $null) {
# do a POST with our headers to the service/login URL, capture the auth token in a session object
  $headers += @{"Authorization"="Basic $($EncodedPassword)"}
  $bearer_token = $global:DefaultCIServers.SessionId
  Write-Host "Bearer Token successfully obtained"
} else {
    Write-host "The session failed to login, please try again"
    exit
}

# Gathering ISOs in the VCD instance
# This could be changed to a specific list if one did not want to eject all ISOs that were in use
$isolist = Search-Cloud adminMedia

Write-Host "**********************************************************************"
Write-host "ISO Count within this instance:" $isolist.Count
Write-Host "**********************************************************************"
$Confirm_eject = "Eject ISO media from all VMs where an ISO is connected?"
$Prompt = "Enter your choice"
$Choices = [System.Management.Automation.Host.ChoiceDescription[]] @("&Yes", "&No")
$Default_choice = 1
$Choice = $host.UI.PromptForChoice($Confirm_eject, $Confirm_eject, $Choices, $Default_choice)

switch($Choice){
    0 {
        foreach ($iso in $isolist){

            $iso.Org -match "urn:vcloud:org:(?<content>.*)"
            $iso_org = $matches['content']
            $iso.id -match "urn:vcloud:media:(?<iso_id>.*)"
    
            $headers =  @{'Authorization'=$bearer_token}
            $headers += @{'x-vmware-vcloud-tenant-context'=$iso_org}
            $headers += @{'Accept'=$accept_header}
            #$iso
            $resource = "/media/" + $matches['iso_id'] + "/attachedVms"
            $url = $baseurl + $resource
            try {
                $response = Invoke-RestMethod -Uri $url -Headers $headers -Method GET -WebSession $MYSESSION
                }
            catch [system.net.webexception] {
                $response = $_.Exception.response
            }
            $eject_vm_href = $response.vmReference.href
            #Checking if there is a VM reference associated with the given ISO
            if ($eject_vm_href -ne $null) {
                Write-Host "**********************************************************************"
                Write-host "The iso" $iso.Name "is connected to the following VM(s)" $response.vmReference.name "and will be ejected"
                Write-Host "**********************************************************************"
    
                foreach ($vm_href in $eject_vm_href){
                    #Build the eject url
                    $eject_resource = "/media/action/ejectMedia"
                    $eject_url = $vm_href + $eject_resource
                    $eject_headers = $headers
                    $eject_headers += @{'Content-Type'='application/*+xml;charset=utf-8'}
                    $eject_body = '<root:MediaInsertOrEjectParams xmlns:root="http://www.vmware.com/vcloud/v1.5"/>'
                    Write-Host "**********************************************************************"
                    Write-Host "Ejecting ISO Media from " $vm_href
                    Write-Host "**********************************************************************"

                    $response = Invoke-RestMethod -Uri $eject_url -Headers $eject_headers -Method POST -WebSession $MYSESSION -Body $eject_body
                }
            } else { 
                Write-host "No VM(s) associated with" $iso.Name
            }
        }
}
    1 {Write-host "There are" $isolist.Count"ISO(s) within this instance. No ISO media will be ejected"}
}