<#
Brandon Bazan https://ifitisnotbroken.wordpress.com/
#This script is designed to gather the Org, vApp, VM to ensure there is only 1 value to consolidate
#It will check the power state since it will only work against powered off VMs
#Assumes that you are already connected to a Cloud Director instance
#>
#User Input:
Write-Host -ForegroundColor Green " `n ****************************************************************"
$org_name = Read-host -Prompt ' Input the Org name'
$vApp_name = Read-host -Prompt ' Input the vApp name'
$VM_input = Read-host -Prompt ' Input the VM name'
Write-Host -ForegroundColor Green " `n ****************************************************************"
$Power_state = Get-Org -Name $org_name | Get-CIVApp -Name $vapp_name | Get-CIVM -Name $VM_input
#Checking power state, if powered off it will consolidate and report progress
If($Power_state.Status -eq 'PoweredOff'){
  $consolidate_vms = Get-Org -Name $org_name | Get-CIVApp -Name $vapp_name | Get-CIVM -Name $VM_input | where {$_.Extensiondata.vCloudExtension.any.VirtualDisksMaxChainLength -ge 2} | Get-CIView

  Foreach ($vm in $consolidate_vms) {   
    Write-Output "Processing VM: $($VM.name)"  
    try {   
        $task = $vm.Consolidate_Task()  
        Start-Sleep 1  
        while ((Get-Task -Id $task.Id).State -ne "Success" ) {  
            #Report power state
            Write-Output "$($vm.name) - Percent: $((Get-Task -Id $task.Id).PercentComplete) / State: $((Get-Task -Id $task.Id).State)"   
            Start-Sleep .5
            }   
        } catch {   
            Write-Output "      $($vm.name) Failed!"   
            }    
   }#End Foreach loop
}elseif($Power_state.status -eq 'PoweredOn'){
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n"
    Write-Host -ForegroundColor Green "  Could not consolidate $VM_input within vApp $vApp_name since it is powered on`n"
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n" 
}elseif($Power_state.Extensiondata.vCloudExtension.any.VirtualDisksMaxChainLength -eq 1){
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n"
    Write-Host -ForegroundColor Green "  Could not consolidate $VM_input within vApp $vApp_name since it has a chain length of 1`n"
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n"
}else{
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n"
    Write-Host -ForegroundColor Green "  Could not consolidate VM $VM_input within vApp $vApp_name`n"
    Write-Host -ForegroundColor Green "  Please check on this VM and run again `n"
    Write-Host -ForegroundColor Yellow " `n **************************************************************** `n"
    }#End else checking if powered off
