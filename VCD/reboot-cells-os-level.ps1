<#PSScriptInfo
.VERSION 1.1
.AUTHOR brandon.bazan at gmail Dot Com
.Blog https://ifitisnotbroken.wordpress.com
.COPYRIGHT
.TAGS
.LICENSEURI
.PROJECTURI
.ICONURI
.EXTERNALMODULEDEPENDENCIES
.REQUIREDSCRIPTS
.EXTERNALSCRIPTDEPENDENCIES
.RELEASENOTES
#>

<#
.DESCRIPTION
Invoke SSH command on all hosts in a given vcloud director instance
.PARAMETER Server
Optional. vCloud Director Server IP/FQDN - if not provided defaults to currently connected vCloud Director Servers
.PARAMETER Username
Required. Short username to connect to vCloud Director / vCenter
.PARAMETER Password
Required. Password for the provided username. Must be provided as SecureString
or entered on the command line if run interactively
.PARAMETER vCDcellPassword
Required. Password for the provided username. Must be provided as SecureString
or entered on the command line if run interactively
.PARAMETER SSHCommand
Required. Command to be executed on vCD cells through SSH
.PARAMETER OutFIle
Optional. File where to optionally write the output of the SSH commands, together with the command exit code
#>

Param(
  [Parameter(Mandatory=$true, HelpMessage="vCloud Director Server IP/FQDN")]
  $Server=$global:DefaultCIServers,
  [Parameter(Mandatory=$true, HelpMessage="vCD Short username")]
  [string] $Username,
  [Parameter(Mandatory=$true, HelpMessage="vCD User password")]
  [Security.SecureString] $Password,
  [Parameter(Mandatory=$true, HelpMessage="Common Linux root password")]
  [Security.SecureString] $vCDcellPassword,
  [Parameter(Mandatory=$true, HelpMessage="File and directory where to write the output of the commands")]
  [String] $OutFile
)

$SSHCommand = "reboot" #this could be changed to restart the service instead of the cell if needed
$SSHCommand1 = "service vmware-vcd status"
#No longer in use -->$SSHCommandOld = "cat /opt/vmware/vcloud-director/logs/cell.log | tail -n 5"
$SSHCommand2 = "cat /opt/vmware/vcloud-director/logs/cell.log | grep -F 'Successfully verified transfer spooling area: /opt/vmware/vcloud-director/data/transfer'"
#Will be in use, working out bugs-->$SSHCommand3 = "/opt/vmware/vcloud-director/bin/cell-management-tool -u $Username -p $Password cell --quiesce true"

$Credential = New-Object System.Management.Automation.PSCredential($Username,$Password)
$cellCredential = New-Object System.Management.Automation.PSCredential("root",$vCDcellPassword)
$ciCredential = $Credential

$ciSession = Connect-CIServer -Server $Server -Credential $ciCredential -NotDefault
Connect-CIServer $Server -Credential $Credential

# Return all cells within the vCD instance
Function Get-VCDCells{
  $cellList = Search-Cloud -QueryType Cell
  return $cellList.name | Sort-Object
}

$cellList = Get-VCDCells -Server $ciSession
if($OutFile){
  $outFileData = @()
}

foreach ($cell in $cellList){
  Write-Host "Rebooting: $cell"
    try{
      $sshSession = New-SSHSession -ComputerName $cell -Credential $cellCredential -AcceptKey
      $cmdOutput = Invoke-SSHCommand -SSHSession $sshSession -Command $SSHCommand -Timeout 5
      [String]$output = $cmdOutput.Output
      Write-host $output
    }
	catch{
      Write-Host $output
    }
   }

Write-Host "Sleeping whilst the cells reboot"
	Start-Sleep 90 #Timer based on the average time it has taken a cell service to fully start
foreach ($cell in $cellList){
  Write-Host "Working with Cell:$cell"
    try{
      $sshSession = New-SSHSession -ComputerName $cell -Credential $cellCredential -AcceptKey
      $cmdOutput = Invoke-SSHCommand -SSHSession $sshSession -Command $SSHCommand1 -Timeout 800
      [String]$output = $cmdOutput.Output
      Write-host $output
	  Write-Host "Checking the results" -ForegroundColor Green
	  $serviceresult = "vmware-vcd-watchdog is running vmware-vcd-cell is running"
	  $service = $output -ceq $serviceresult
	  if($service)
	  {
	  Write-Host "The vCD service started properly on $cell" -ForegroundColor Green
	  } Else {
	  Write-Host "The vCD service did not start properly please check $cell" -ForegroundColor Red
	  }

      if($OutFile){
        $outputObject = new-object PSObject
        $outputObject | add-member -membertype NoteProperty -name "hostname" -Value $cell
        $outputObject | add-member -membertype NoteProperty -name "output" -Value $output
        $outputObject | add-member -membertype NoteProperty -name "exit_code" -Value $cmdOutput.ExitStatus
	$outputObject | add-member -membertype NoteProperty -name "transfer_mounted_vcd_started" -Value $service
        $outFileData += $outputObject
      }
    }catch{
      Write-Host "Failed to execute $SSHCommand1 on $cell"
    }
   }

foreach ($cell in $cellList){
  Write-Host "Working with Cell:$cell"

    try{
      $sshSession = New-SSHSession -ComputerName $cell -Credential $cellCredential -AcceptKey
      $cmdOutput = Invoke-SSHCommand -SSHSession $sshSession -Command $SSHCommand2 -Timeout 800
      [String]$output = $cmdOutput.Output
      Write-host $output
	  Write-Host "Checking the results" -ForegroundColor Green
	  $results = "Successfully verified transfer spooling area: /opt/vmware/vcloud-director/data/transfer"
	  $transfer = $output -ceq $results
	  if($transfer)
	  {
	  Write-Host "The transfer server mapped properly" -ForegroundColor Green
	  } Else {
	  Write-Host "The transfer server did not map properly please check $cell" -ForegroundColor Red
	  }

      if($OutFile){
        $outputObject = new-object PSObject
        $outputObject | add-member -membertype NoteProperty -name "hostname" -Value $cell
        $outputObject | add-member -membertype NoteProperty -name "output" -Value $output
        $outputObject | add-member -membertype NoteProperty -name "exit_code" -Value $cmdOutput.ExitStatus
	$outputObject | add-member -membertype NoteProperty -name "transfer_mounted_vcd_started" -Value $transfer
        $outFileData += $outputObject
      }
    }catch{
      Write-Host "Failed to execute $SSHCommand2 on $cell"
    }
   }

Disconnect-CIServer -Server $ciSession -Confirm:$false #Disconnects the vCD session

if($OutFile){
  try{
    $outFileData | Export-Csv -Path $OutFile
    Write-Host "Data written to $OutFile" -ForegroundColor Green
  }catch{
    Write-Host "Unable to write the data to $OutFile" -ForegroundColor Red
  }
}