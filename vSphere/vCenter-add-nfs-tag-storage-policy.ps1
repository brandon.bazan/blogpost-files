#This assumes that a connection to vCenter has been established. The logic will be added later
$catname = 'catalog'
$tagname = 'catalog'
$dsname = 'bblab-catalog1'
$nfshost = 'some-nfs-host'
$nfspath = 'some-nfs-mount-path'
$clustername = 'a lovely cluster name here'
$spname = 'catalog'
$spdescription = 'Used for Catalog NFS'
New-TagCategory -Name $catname -Description 'Used for NFS Mounts with VCD'
$newcat = Get-TagCategory -Name $catname
New-Tag -Name $tagname -Category $newcat
New-SpbmStoragePolicy -Name $spname -Description $spdescription -AnyOfRuleSets (New-SpbmRuleSet (New-SpbmRule -Tag $newtagname))
$newtagname = Get-Tag -Name $tagname
#$hostlist = Get-VMHost | select -First 1
get-cluster $clustername | get-vmhost | New-Datastore -Name $dsname -Path $nfspath -Nfs -NfsHost $nfshost
$dstag = Get-Datastore -Name $dsname
New-TagAssignment -Tag $newtagname -Entity $dstag