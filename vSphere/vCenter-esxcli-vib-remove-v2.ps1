﻿<#

.SYNOPSIS
This script uses get-esxcli to remove VIB(s) from esxi hosts in a vCenter

.DESCRIPTION
The script will ask for input of vCenter connection information and VIB(s) you wish to remove
It will then loop over the hosts in the vCenter to remove the VIB(s)

.EXAMPLE
.\vCenter-esxcli-rib-remove-ps1

.NOTES
Brandon Bazan
Remove VIB(s)

.LINK
http://ifitisnotbroken.wordpress.com

#>
[CmdletBinding(DefaultParameterSetName = "All")]
PARAM (
    [parameter(Mandatory = $true)]
    [pscredential]
    $Credential,
    [parameter(Mandatory = $true)]
    $vCenterServer
)
Write-Host -ForegroundColor Yellow " These options will be passed for ALL VIBs entered below!"
$dryrun = Read-host -Prompt ' Dry Run?(TRUE/FALSE)'
$dryrun = [System.Convert]::ToBoolean($dryrun)
$maintenancemode = Read-host -Prompt ' Have each host enter MaintenanceMode?(TRUE/FALSE)'
$maintenancemode = [System.Convert]::ToBoolean($maintenancemode)
$noliveinstall = Read-host -Prompt ' No Live Install?(TRUE/FALSE)'
$noliveinstall = [System.Convert]::ToBoolean($noliveinstall)
$force = Read-host -Prompt ' Force remove VIB?(TRUE/FALSE)'
$force = [System.Convert]::ToBoolean($force)
Connect-VIServer -Server $vCenterServer -Credential $Credential

$vibarray = @()
    do {
        $input = (Read-Host "Please enter the name of the VIB(s) you want to remove, to complete the list do not enter a value and press enter")
        if ($input -ne '') {$vibarray += $input}
    }#End do statement
    until ($input -eq '')#Loop will stop when user enter '' as input
#Gathers connected hosts that are not in maintenance mode
$hosts = Get-VMHost | Where-Object { $_.ConnectionState -eq "Connected" }

ForEach($vibname in $vibarray) {
    ForEach($vibhost in $hosts){
        Write-Host "Working on $vibhost and removing $vibname"
        #Exposes the ESX CLI functionality of the current host
        $ESXCLI = Get-EsxCli -VMHost $vibhost -V2
        $VIBArguments = $esxcli.software.vib.remove.CreateArgs()
        $VIBArguments.dryrun = $dryrun
        $VIBArguments.maintenancemode = $maintenancemode
        $VIBArguments.noliveinstall = $noliveinstall
        $VIBArguments.force = $force
        $VIBArguments.vibname = $vibname
        $esxcli.software.vib.remove.Invoke($VIBArguments)
        #esxcli.software.vib.remove(boolean dryrun, boolean force, boolean maintenancemode, boolean noliveinstall, string[] vibname)
        #The above line describes the values
    }#end ForEach vibhost
}#end ForEach vibname