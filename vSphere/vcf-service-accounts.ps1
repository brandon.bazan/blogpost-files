#This script assumes that we have already gotten a list of esxi hosts by connecting to vC or feeding in a CSV. The following command could be run

#$ESXhost =  Get-VMHost 

$NewUserPassword = 'NewSVCAccountPassword'
$NewUserDesc = 'VCF Service Account'
$HOSTUser = 'root'

$HostPassword = 'ESXiRootPasswordHere'

foreach($hostinlist in $ESXhost){
    Connect-VIServer -Server  $hostinlist -User $HOSTUser -Password $HostPassword
    $esxcli2 = Get-EsxCli -V2
    $vcfhostname = $esxcli2.VMHost.NetworkInfo.HostName
    $vcfservice = 'svc-vcf-'
    $fullaccount = $vcfservice + $vcfhostname

    New-VMHostAccount -Id $fullaccount -Password $NewUserPassword -Description $NewUserDesc -UserAccount

    $esxcliArgs = $esxcli2.system.permission.set.CreateArgs()
# Hard coding the new user role to Admin and group to false
    $esxcliArgs.role = "Admin"
    $esxcliArgs.group = $false

    $esxcliArgs.id = $fullaccount
    $esxcli2.system.permission.set.Invoke($esxcliArgs)

    Disconnect-VIServer * -Confirm:$false
# Resetting variables for each server in the list
    $esxcliArgs.id = $null
    $fullaccount = $null
    $esxcli2 = $null
    $hostinlist = $null
}
