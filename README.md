# Used for https://ifitisnotbroken.wordpress.com blogpost files
# Use at your own risk

#vCD Cell Reboot
    -Used to reboot all cells in a given vCD instance
    -No current option to do rolling reboots(feature to be added)
    

#vCD Catalog Powershell Module

This was developed off of Jon Waite's original version of [New-Catalog](https://github.com/jondwaite/New-Catalog)

I have since added features:
    -Allow creation of a new subscribed catalog
    -Allow creation of published catalog
    -Modify existing catalog to make it published
